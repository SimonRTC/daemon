const path = require('path');

var config = {
    mode: 'production',
    module: {},
    optimization: {
        minimize: true,
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
};

var server = Object.assign({}, config, {
    name: "server-side",
    entry: "./src/server/main.ts",
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: "bundle.server.js"
    },
});
var client = Object.assign({}, config,{
    name: "client-side",
    entry: "./src/client/main.ts",
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: "bundle.client.js"
    },
});

module.exports = [ server, client ];