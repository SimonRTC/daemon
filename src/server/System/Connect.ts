const slots     = GetConvar("sv_maxclients", -1);
const Delay     = (ms) => new Promise(res => setTimeout(res, ms));
let WaitingList = {};

on('playerConnecting', (name, setKickReason, deferrals) => {
    const source = global.source;
    deferrals.defer();
    deferrals.update("Perform the usual checks...");
    setTimeout(async () => {
        if (slots >= 1) {
            let PlayerIndices = GetNumPlayerIndices();
            if (slots > PlayerIndices) {                
                deferrals.done();
            } else {
                let timer           = 0;
                let ready           = false;
                let ListCount       = Object.keys(WaitingList).length;
                WaitingList[source] = {
                    location: ListCount + 1,
                    timeout: 5
                };
                while (true) {
                    if (ready === false) {
                        deferrals.update(`You are ${WaitingList[source].location}/${ListCount} in the queue. You have been waiting for ${timer} seconds.`);
                        if (WaitingList[source].timeout >= 10) {
                            WaitingList[source].timeout = 5
                        } else {
                            if (GetPlayerGuid(source)) {
                                Object.keys(WaitingList).forEach((PlayerSrc, index) => {
                                    if (PlayerSrc == source) {
                                        WaitingList[source].location = index + 1;
                                        if (WaitingList[source].location === 1) {
                                            PlayerIndices = GetNumPlayerIndices();
                                            if (slots > PlayerIndices) {
                                                deferrals.done();
                                                ready = true;
                                            }
                                        }
                                    }
                                });
                                WaitingList[source].timeout++;
                            }
                        }
                        timer++;
                        ListCount = Object.keys(WaitingList).length;
                        await Delay(1000);
                    } else {
                        delete WaitingList[source];
                        break;
                    }
                }
            }
        } else {
            deferrals.done("No player limit has setted. (please contact dev teams)");
        }
    }, 0);
});

setTimeout(AutoCleanWaitingList, 1000);
async function AutoCleanWaitingList() {
    Object.keys(WaitingList).forEach(source => {
        if (WaitingList[source].timeout <= 0) {
            DropPlayer(source, "Connection to server interrupted for more than ~5 seconds.");
            delete WaitingList[source];
        } else {
            WaitingList[source].timeout--;
        }
    });
    setTimeout(AutoCleanWaitingList, 1200);
    return;
}